#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QTreeView>
#include <QFileSystemModel>
#include <QVBoxLayout>
#include <QString>

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    QLineEdit *m_lineEdit;
    QTreeView *m_treeView;
    QFileSystemModel *m_model;
    QVBoxLayout *m_layout;
    QFlags<QDir::Filter> m_filter;

private slots:
    void EnterPressed();
};
#endif // WIDGET_H
