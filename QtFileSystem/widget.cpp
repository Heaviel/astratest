#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    this->m_lineEdit = new QLineEdit();
    this->m_treeView = new QTreeView();
    this->m_model = new QFileSystemModel();
    this->m_layout = new QVBoxLayout(this);

    this->m_filter = QDir::Filter::Dirs |
            QDir::Filter::Files |
            QDir::Filter::Hidden;

    this->m_model->setRootPath(QDir::homePath());
    this->m_model->setFilter(m_filter);
    this->m_model->setNameFilterDisables(false);

    this->m_treeView->setModel(this->m_model);
    this->m_treeView->setRootIndex(this->m_model->index(QDir::homePath()));

    m_layout->addWidget(m_lineEdit);
    m_layout->addWidget(m_treeView);

    connect(m_lineEdit, &QLineEdit::returnPressed, this, &Widget::EnterPressed);

    this->resize(800, 600);
}

Widget::~Widget()
{
    delete this->m_lineEdit;
    delete this->m_model;
    delete this->m_layout;
    delete this->m_treeView;
}


void Widget::EnterPressed()
{
    QStringList m_words = {};
    if (this->m_lineEdit->text() != "") {
        m_words << this->m_lineEdit->text();
    }
    this->m_model->setNameFilters(m_words);
    this->repaint();
}

